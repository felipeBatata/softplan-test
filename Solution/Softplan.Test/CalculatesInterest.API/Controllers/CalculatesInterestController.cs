﻿using System;
using System.Threading.Tasks;
using Interest.Application.Features.Commands;
using Interest.Application.Features.Interest.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CalculatesInterest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculatesInterestController : ControllerBase
    {
        private readonly IInterestService _interestService;

        public CalculatesInterestController(IInterestService interestService) : base()
        {
            _interestService = interestService;
        }

        [ProducesResponseType(typeof(decimal), 200)]
        [ProducesResponseType(typeof(Exception), 400)]
        [HttpGet("calculajuros")]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromBody] CalculatesInterestCommand command)
        {
            var result = await _interestService.CalculateInterest(command);

            return Ok(result);
        }

        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(Exception), 400)]
        [HttpGet("showmethecode")]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            return Ok("https://gitlab.com/felipeBatata/softplan-test");
        }
    }
}
