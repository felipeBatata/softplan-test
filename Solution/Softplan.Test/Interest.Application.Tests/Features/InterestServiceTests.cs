﻿using FluentAssertions;
using Interest.Application.Features.Commands;
using Interest.Application.Features.Interest.Services;
using Interest.Application.Features.Interest.Services.API;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interest.Application.Tests.Features
{
    [TestFixture]
    public class InterestServiceTests
    {
        private InterestService _interestService;
        private Mock<IApiRequestService> _apiRequestService;

        [SetUp]
        public void Initialize()
        {
            _interestService = new InterestService();

            _apiRequestService = new Mock<IApiRequestService>();
            _interestService.ApiRequestService = _apiRequestService.Object;
        }

        [Test]
        public async Task Deveria_calcular_taxa_de_juros_com_sucesso()
        {
            //Arrange
            _apiRequestService.Setup(x => x.GetResultAPI()).Returns("0.02");
            var command = new CalculatesInterestCommand { InitialValue = 100,Months = 5 };

            //Action
            var request = _interestService.CalculateInterest(command);

            //Assert
            request.Should().NotBeNull();
            request.Result.Should().Be("R$200,83");
        }

        [Test]
        public async Task Deveria_calcular_taxa_de_juros_com_erro()
        {
            //Arrange
            _apiRequestService.Setup(x => x.GetResultAPI()).Returns("");
            var command = new CalculatesInterestCommand { InitialValue = 100, Months = 5 };

            //Action
            var request = _interestService.CalculateInterest(command);

            //Assert
            request.Should().NotBeNull();
            request.Result.Should().Be("Não foi possível retornar o valor da taxa de juros");
        }
    }
}
