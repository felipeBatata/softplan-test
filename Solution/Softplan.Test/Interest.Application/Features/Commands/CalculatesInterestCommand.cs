﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interest.Application.Features.Commands
{
    public class CalculatesInterestCommand
    {
        public decimal InitialValue { get; set; }

        public int Months { get; set; }
    }

    public class CalculatesInterestCommandValidator : AbstractValidator<CalculatesInterestCommand>
    {
        public CalculatesInterestCommandValidator()
        {
            RuleFor(c => c.InitialValue).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(c => c.Months).NotNull().NotEmpty().GreaterThan(0);
        }
    }
}
