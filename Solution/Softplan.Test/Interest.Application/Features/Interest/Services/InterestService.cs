﻿using Interest.Application.Features.Commands;
using Interest.Application.Features.Interest.Services.API;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace Interest.Application.Features.Interest.Services
{
    public class InterestService : IInterestService
    {
        public IApiRequestService ApiRequestService { get; set; }

        public InterestService()
        {
            ApiRequestService = new ApiRequestService();
        }

        public async Task<string> CalculateInterest(CalculatesInterestCommand calculatesInterest)
        {
            return await Calculates(calculatesInterest);
        }

        private async Task<string> Calculates(CalculatesInterestCommand calculatesInterest)
        {
            double iniitalValue = Convert.ToDouble(calculatesInterest.InitialValue);
            int installments = calculatesInterest.Months;
            double interestBase = await GetInterestBaseFromAPI();

            if (interestBase > 0)
            {
                var result = (iniitalValue * Math.Pow((1 + interestBase), installments) * interestBase) / (Math.Pow((1 + interestBase), installments) - 1);

                return result.ToString("C2");
            }
            else
                return "Não foi possível retornar o valor da taxa de juros";
        }

        private async Task<double> GetInterestBaseFromAPI()
        {
            var result = ApiRequestService.GetResultAPI();

            if (!string.IsNullOrEmpty(result))
              return  Convert.ToDouble(result);
            else
                return 0;
        }

    }
}
