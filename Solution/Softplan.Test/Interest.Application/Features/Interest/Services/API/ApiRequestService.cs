﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace Interest.Application.Features.Interest.Services.API
{
   public  class ApiRequestService : IApiRequestService
    {
        private HttpClient CreateHeaderHttpClient()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri("http://localhost:9001/");

                return client;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetResultAPI()
        {
            HttpClient client = CreateHeaderHttpClient();

            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback
               (
                  delegate { return true; }
               );

            Task<HttpResponseMessage> getResponse = Task.Run(async () => await client.GetAsync($"{"api/InterestRate"}{"/taxaJuros"}"));

            if (getResponse.Result.IsSuccessStatusCode)
            {
                using (HttpContent contentResponse = getResponse.Result.Content)
                {
                    Task<string> taskRead = contentResponse.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<string>(taskRead.Result);
                }
            }

            return null;
        }
    }
}
