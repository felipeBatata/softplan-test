﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interest.Application.Features.Interest.Services.API
{
    public interface IApiRequestService
    {
        string GetResultAPI();
    }
}
