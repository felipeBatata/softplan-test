﻿using Interest.Application.Features.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interest.Application.Features.Interest.Services
{
    public interface IInterestService
    {
        Task<String> CalculateInterest(CalculatesInterestCommand calculatesInterest);
    }
}
