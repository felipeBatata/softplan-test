﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InterestRate.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InterestRateController : ControllerBase
    {
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(Exception), 400)]
        [HttpGet("taxaJuros")]
        [AllowAnonymous]
        public ActionResult<string> Get()
        {
            return "0,01";
        }
    }
}
